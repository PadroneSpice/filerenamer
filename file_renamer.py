### Custom File Renamer
### Sean Castillo
import os
import csv
import pathlib

def loadNames(fileToParse):
    pairList = []
    with open (fileToParse) as fd:
        rd = csv.reader(fd, delimiter=",", quotechar='"')
        for row in rd:
            pairList.append(row)
    return pairList

def main():
    inputFile = "rename_pairs.csv"
    PATH = os.getcwd()
    pairs = loadNames(inputFile)


    for p in pairs:
        location = PATH
        location +='\\'
        location += p[0]

        if (os.path.isfile(p[0])):
            print("File found: " + p[0])
            os.rename(p[0], p[1])
        else:
            print("File not found: " + p[0])

if __name__ == '__main__':
    main()
